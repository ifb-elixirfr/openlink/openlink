# User Documentation

The web application Openlink makes it possible to establish a logical link between the various media used by research teams. To us, it seems to be a primary need to assist the researchers with the monitoring and the enrichment of the metadata throughout the data lifecycle.

## Glossary

*API*: Application programming interface, a software intermediary that allows two applications to talk to each other.<br>
*Assay*: An experiment, a measurement or a model.<br>
*Asynchronous tasks*: Tasks operate independently of other processes.<br>
*Connector*: A set of classes, functions and variables to describe how Openlink interacts with a tools API.<br>
*Data management Tools*: Tool used to organize, process and analyze an organization data.<br>
*Dataset*: A collection of related data, produced under an experimental protocol.<br>
*Project*: Collaborative work on a research subject.<br>
*Investigation*: The main objective of a project.<br>
*Study*: A biological hypothesis to be tested in different ways.<br>


## Project list

The Openlink project list shows an inventory of your projects.  It'll display the name, creation date, project author and a button to delete it. Openlink gives the possibility to share a project with other users. The projects you own and the project's shared with you appear in the same list.

![Project_list](img/Screenshot_1.png)


## Project page

Openlink shows a dashboard that centralizes links to datasets, structured in an ISA model.
The [ISA model](https://isa-specs.readthedocs.io/en/latest/isamodel.html) consists of three core entities to capture experimental metadata: Investigation, Study and Assay.
Openlink performs mappings between "elements" from a tool commonly used by researchers (lab notebook, storage infrastructure, analysis platforms, etc.) and the ISA model.
Once you have created an investigation, you can manually create a Study by clicking the investigation’s “+” button and then manually create an Assay  by clicking the Study’s “+” button.

![Project_page](img/Screenshot_2.png)

From the project page, you can access different interfaces: “Manage Users”, “Manage Tools” and “Asynchronous Tasks”.


## Manage user

You can add users to your project. They will be able to map or publish data if they have permission to do so:
There are 3 access rights levels:
- Administrator
- Contributor (he can’t delete the project nor invite more user)
- Collaborator (he can’t delete or edit anything)

![Manage_user](img/Screenshot_3.png)


## Manage tools

Openlink is able to connect to several data management tools via connectors.
These are modular. Indeed, you can easily add new ones in the form of Python packages.
There are currently 6 connectors available:
Labguru, an electronic lab notebook
Seafile, an open source file synchronization and sharing solution
Galaxy, a workflow manager
SSH, a network protocol giving users a secure way to access a computing infrastructure.
OMERO, an image data visualization, management and processing tool
Zenodo, a universal repository for research outcomes.
To sum up, you can declare tools with which openlink will be able to communicate. There are 5 mappers and 1 Publisher:

![Manage_tools](img/Screenshot_4.png)


## Asynchronous tasks

Asynchronous Tasks, from the project page, displays the latest asynchronous tasks to monitor ongoing tasks. After 30 minutes, the task will disappear from the table. Nevertheless, you can click on “All asynchronous tasks” to access the full list of tasks launched for this project.

![Asynchronous_tasks](img/Screenshot_5.png)


## Page from an element of the ISA model

Here you can not only edit the name and description of the element but also map it to one or several links (“add link” button) and publish them (“Publish” button).

![ISA_model_Openlink](img/Screenshot_6.png)

Once a link has been added, the button “view on …” will open a new window showing the mapped element whereas the button “Add & link” gives the possibility to map child elements from the parent page.