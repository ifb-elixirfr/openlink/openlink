import io
import logging
import os
import socket
from stat import S_ISDIR, S_ISREG
from openlink.core.lib import utils
import openlink.core.connector
import paramiko
from django import forms
from openlink.core.connector import (
    ContainerDataObject,
    DataObject,
    Mapper,
    ToolConnector,
    ToolForm,
)

logger = logging.getLogger(__name__)


message = """<h5>How to get ssh private and public keys and set it up on your server?</h5>
    <ol>
        <li>Before creating your SSH key, you should first check to make sure you haven't already created one in the past:</li>
        <li><i>cd ~/.ssh</i></li>
        <li><i>ls</i></li>
        <li>You should look for a pair of files named id_rsa.pub and id_rsa. These are known as your public and private keys respectively.</li>
        <li>To create your SSH key, run the following command:</li>
        <li><i>ssh-keygen</i></li>
        <li>Install your ssh public key on a server as an authorized key:</li>
        <li><i>ssh-copy-id -i ~/.ssh/mypubkey user@host</i></li>
        <li>Copy SSH private key:
        <br><i>alias pbcopy="xclip -sel clip"</i>
        <br><i>cat id_rsa | pbcopy</i>
        </li>
    </ol>
    """


class SSHForm(ToolForm):
    url = forms.CharField(
        label="Hostname",
        help_text="example: hpc.hostname.com",
        max_length=100,
        required=True,
    )
    username = forms.CharField(label="Username", max_length=100, required=True)
    private_key = forms.CharField(
        widget=forms.Textarea, label="Private key", help_text=message, required=True
    )
    shared = forms.BooleanField(
        label="Allows asynchronous tasks",
        help_text="This option allows openlink to access your login and password while not connected to run multiple asynchronous tasks",
        required=False,
    )
    public_data_dict = {}
    private_data_dict = {}

    def clean(self):
        cleaned_data = super().clean()
        hostname = cleaned_data.get("url")
        username = cleaned_data.get("username")
        private_key = cleaned_data.get("private_key")
        cleaned_data["private_key"] = cleaned_data.pop("private_key")
        SSHConnector.create_session(username, hostname, private_key)

        public_data_list = ("url", "")
        private_data_list = ("username", "private_key")

        self.public_data_dict = {
            k: cleaned_data[k] for k in public_data_list if k in cleaned_data
        }
        self.private_data_dict = {
            k: cleaned_data[k] for k in private_data_list if k in cleaned_data
        }
        return cleaned_data


class SSHEditCredentials(ToolForm):
    url = forms.CharField()
    username = forms.CharField(label="Username", max_length=100, required=True)
    private_key = forms.CharField(
        widget=forms.Textarea, label="Private key", help_text=message, required=True
    )
    shared = forms.BooleanField(
        label="Allows asynchronous tasks",
        help_text="This option allows openlink to access your login and password while not connected to run multiple asynchronous tasks",
        required=False,
    )
    private_data_dict = {}

    def __init__(self, data, *args, **kwargs):
        url = kwargs["url"]
        kwargs.pop("url")
        super(SSHEditCredentials, self).__init__(data)
        self.fields["url"] = forms.URLField(
            label="SSH URL", max_length=100, required=True, initial=url, disabled=True
        )

    def clean(self):
        cleaned_data = super().clean()
        hostname = cleaned_data.get("url")
        username = cleaned_data.get("username")
        private_key = cleaned_data.get("private_key")
        cleaned_data["private_key"] = cleaned_data.pop("private_key")
        SSHConnector.create_session(username, hostname, private_key)
        private_data_list = ("username", "private_key")
        self.private_data_dict = {
            k: cleaned_data[k] for k in private_data_list if k in cleaned_data
        }

        return cleaned_data


class SSHConnector(Mapper):
    def __init__(self, tool, token, user_vault_id=None):
        self.url = tool.get_public_param("url")
        self.username = tool.get_private_param("username", token, user_vault_id)
        self.private_key = tool.get_private_param("private_key", token, user_vault_id)
        self.tool = tool

    @classmethod
    def get_name(cls):
        return "SSH"

    @classmethod
    def get_data_structure(cls):
        return openlink.core.connector.TREE_STRUCTURE

    @classmethod
    def has_access_url(cls):
        return False

    @classmethod
    def get_addlink_form(cls, data, *args, **kwargs):
        return SSHEditCredentials(data, *args, **kwargs)

    @classmethod
    def get_supported_types(cls, data_object=None, tool=None):
        list_supported_types = {"assay": ["folder"], "data": ["folder", "file"]}
        return list_supported_types

    def get_plural_form_of_type(cls, type):
        plural_form = {"folder": "folders", "file": "files"}
        return plural_form[type]

    @classmethod
    def get_creation_form(cls, data=None):
        return SSHForm(data)

    @classmethod
    def get_logo(cls):
        logo = "images/logo-ssh.jpeg"
        return logo

    @classmethod
    def get_color(cls):
        color = "#C3C6C4"
        return color

    @classmethod
    def create_session(cls, username, hostname, private_key):
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        key_file = io.StringIO(private_key)
        key = paramiko.RSAKey.from_private_key(key_file)
        try:
            client.connect(hostname, username=username, pkey=key)
        except (
            paramiko.ssh_exception.BadHostKeyException,
            paramiko.ssh_exception.AuthenticationException,
            paramiko.ssh_exception.SSHException,
            socket.error,
        ) as ssh_exception:
            logger.debug("Warning: SSH connection error. (This could be temporary.)")
            logger.debug("Host: " + hostname)
            logger.debug("Error: " + ssh_exception)
        SFTP_client = client.open_sftp()
        return SFTP_client

    @classmethod
    def has_mapping_options(cls):
        return False

    def get_url_link_to_an_object(self, obj_type, obj_id):
        # return a url to display for current user
        return ""

    def get_space_info(self, objects_id, mapping_object=None):
        if mapping_object:
            jobid = utils.start_async_task()
        try:
            total_size = 0
            client = SSHConnector.create_session(self.username, self.url, self.private_key)
            for object_id in objects_id:
                entry = client.lstat(object_id)
                mode = entry.st_mode
                if S_ISREG(mode):
                    total_size += int(entry.st_size)
                if S_ISDIR(mode):
                    total_size += self.get_dir_size(client, object_id)
            if mapping_object:
                mapping_object.size = total_size
                mapping_object.save()
                utils.finish_async_task(jobid, total_size)
            else:
                return total_size
        except Exception as e:
            utils.error_async_task(jobid)
            return e

    def get_dir_size(self, client, dir_path):
        total_size = 0
        for file in client.listdir_attr(dir_path):
            mode = file.st_mode
            if S_ISREG(mode):
                total_size += file.st_size
            if S_ISDIR(mode):
                total_size += self.get_dir_size(
                    client, os.path.join(dir_path, file.filename)
                )
        return total_size

    def get_investigation(self, obj_id):
        return None

    def get_study(self, obj_id):
        return None

    def get_assay(self, obj_id):
        name = obj_id.rsplit("/")[-2]
        obj_to_link = ContainerDataObject(obj_id, name, "folder")
        return obj_to_link

    def get_data(self, obj_id):
        name = obj_id.rsplit("/")[-1]
        obj_to_link = DataObject(obj_id, name, "file")
        if name == "":
            name = obj_id.rsplit("/")[-2]
            obj_to_link = DataObject(obj_id, name, "folder")
        return obj_to_link

    def get_investigations(self):
        return []

    def get_studies(self, reference_mapping, container):
        return []

    def get_assays(self, reference_mapping, container):
        items = []
        client = SSHConnector.create_session(self.username, self.url, self.private_key)
        if container is not None:
            path = reference_mapping.id
            for entry in client.listdir_attr(path):
                mode = entry.st_mode
                if S_ISDIR(mode):
                    items.append(
                        ContainerDataObject(
                            path + entry.filename + "/", entry.filename, "folder"
                        )
                    )
                if S_ISREG(mode):
                    items.append(
                        DataObject(path + entry.filename, entry.filename, "file")
                    )
        else:
            items.append(ContainerDataObject("/", "", "folder"))
        return items

    def get_datas(self, reference_mapping, container):
        items = []
        client = SSHConnector.create_session(self.username, self.url, self.private_key)
        if container is not None:
            path = container.id
            for entry in client.listdir_attr(path):
                mode = entry.st_mode
                if S_ISDIR(mode):
                    items.append(
                        ContainerDataObject(
                            path + entry.filename + "/", entry.filename, "folder"
                        )
                    )
                if S_ISREG(mode):
                    items.append(
                        DataObject(path + entry.filename, entry.filename, "file")
                    )
        else:
            items.append(ContainerDataObject("/", "", "folder"))
        return items

    def get_dir(self, object_id):
        items = []
        client = SSHConnector.create_session(self.username, self.url, self.private_key)
        if object_id.endswith("/"):
            path = object_id
            for entry in client.listdir_attr(path):
                mode = entry.st_mode
                if S_ISDIR(mode):
                    items.append(
                        ContainerDataObject(
                            path + entry.filename + "/", entry.filename, "folder"
                        )
                    )
                if S_ISREG(mode):
                    items.append(
                        DataObject(path + entry.filename, entry.filename, "file")
                    )
        return items

    def get_data_objects(self, data_type, reference_mapping=None, container=None):
        if data_type == "investigation":
            get_objects = self.get_investigations()
        elif data_type == "study":
            get_objects = self.get_studies(reference_mapping, container)
        elif data_type == "assay":
            get_objects = self.get_assays(reference_mapping, container)
        elif data_type == "data":
            get_objects = self.get_datas(reference_mapping, container)
        return get_objects

    def get_data_object(self, data_type, obj_id):
        if data_type == "investigation":
            get_object = self.get_investigation(obj_id)
        elif data_type == "study":
            get_object = self.get_study(obj_id)
        elif data_type == "assay":
            get_object = self.get_assay(obj_id)
        elif data_type == "data":
            get_object = self.get_data(obj_id)
        return get_object

    def get_information(self, type, id):
        return {}

    def download(self, object_id, path):
        client = SSHConnector.create_session(self.username, self.url, self.private_key)
        entry = client.lstat(object_id)
        mode = entry.st_mode
        if S_ISREG(mode):
            name = object_id.rsplit("/")[-1]
            data_dir = os.path.join(path, name)
            client.get(remotepath=object_id, localpath=data_dir)
        if S_ISDIR(mode):
            name = object_id.rsplit("/")[-2]
            dir_path = os.path.join(path, name)
            try:
                os.chdir(dir_path)
            except OSError:
                os.mkdir(dir_path)
            self.download_dir(client, object_id, dir_path)
        client.close
        return None

    def download_dir(self, client, object_dir, dir_path):
        for file in client.listdir_attr(object_dir):
            remotepath = object_dir + "/" + file.filename
            localpath = os.path.join(dir_path, file.filename)
            mode = file.st_mode
            if S_ISDIR(mode):
                try:
                    os.chdir(localpath)
                except OSError:
                    os.mkdir(localpath)
                self.download_dir(client, remotepath, localpath)
            elif S_ISREG(mode):
                client.get(remotepath=remotepath, localpath=localpath)

    def check_file_access(self, object_id):
        client = SSHConnector.create_session(self.username, self.url, self.private_key)
        try:
            client.lstat(object_id)
            access = True
        except Exception as e:
            logging.debug(e)
            access = False
        client.close
        return access


ToolConnector.register(SSHConnector)
