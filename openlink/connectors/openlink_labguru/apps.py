from django.apps import AppConfig


class OpenlinkLabguruConfig(AppConfig):
    name = "openlink_labguru"
