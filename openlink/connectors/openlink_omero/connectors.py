# from django.shortcuts import render
import logging
import os
from time import sleep
from urllib.parse import urlparse
from openlink.core.lib import utils
import omero
import openlink.core.connector
from django import forms
from Glacier2 import PermissionDeniedException
from Ice import DNSException
from omero.cli import cli_login
from omero.gateway import BlitzGateway
from openlink.core.connector import (
    AuthentificationError,
    ContainerDataObject,
    DataObject,
    Mapper,
    ToolConnector,
    ToolForm,
    ToolUnreachableError,
    defaultError,
)
from openlink.core.models import Mapping

logger = logging.getLogger(__name__)


class OmeroForm(ToolForm):
    url = forms.URLField(
        label="Omero Host URL",
        help_text="example: https://url_omero.com",
        max_length=100,
        required=True,
    )
    login = forms.CharField(label="Login", max_length=100, required=True)
    password = forms.CharField(
        widget=forms.PasswordInput, label="Password", max_length=100, required=True
    )
    shared = forms.BooleanField(
        label="Allows asynchronous tasks",
        help_text="This option allows openlink to access your login and password while not connected to run multiple asynchronous tasks",
        required=False,
    )
    public_data_dict = {}
    private_data_dict = {}

    def __init__(self, *args, **kwargs):
        super(OmeroForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        url = cleaned_data.get("url")
        login = cleaned_data.get("login")
        password = cleaned_data.get("password")

        omero_api_url = urlparse(url)
        try:
            omero_client = omero.client(omero_api_url.hostname, omero_api_url.port)
            omero_client.createSession(login, password)
        except Exception as e:
            try:
                OmeroConnector.check_status_code(e)
            except AuthentificationError as e:
                self.add_error("login", str(e))
                self.add_error("password", str(e))
            except ToolUnreachableError:
                self.add_error("url", "invalid seafile URL or server not reachable")
        else:
            omero_client.closeSession()

        public_data_list = ("url", "")
        private_data_list = ("login", "password")

        self.public_data_dict = {
            k: cleaned_data[k] for k in public_data_list if k in cleaned_data
        }
        self.private_data_dict = {
            k: cleaned_data[k] for k in private_data_list if k in cleaned_data
        }

        return cleaned_data


class OmeroEditCredentials(ToolForm):
    url = forms.URLField()
    login = forms.CharField(label="Login", max_length=100, required=True)
    password = forms.CharField(
        widget=forms.PasswordInput, label="Password", max_length=100, required=True
    )
    shared = forms.BooleanField(
        label="Allows asynchronous tasks",
        help_text="This option allows openlink to access your login and password while not connected to run multiple asynchronous tasks",
        required=False,
    )
    private_data_dict = {}

    def __init__(self, data, *args, **kwargs):
        url = kwargs["url"]
        kwargs.pop("url")
        super(OmeroEditCredentials, self).__init__(data)
        self.fields["url"] = forms.URLField(
            label="Omero URL", max_length=100, required=True, initial=url, disabled=True
        )

    def clean(self):
        cleaned_data = super().clean()
        url = cleaned_data.get("url")
        login = cleaned_data.get("login")
        password = cleaned_data.get("password")

        omero_api_url = urlparse(url)
        try:
            omero_client = omero.client(omero_api_url.hostname, omero_api_url.port)
            omero_client.createSession(login, password)
        except Exception as e:
            try:
                OmeroConnector.check_status_code(e)
            except AuthentificationError as e:
                self.add_error("login", str(e))
                self.add_error("password", str(e))
            except ToolUnreachableError:
                self.add_error("url", "invalid omero URL or server not reachable")
        else:
            omero_client.closeSession()
        private_data_list = ("login", "password")
        self.private_data_dict = {
            k: cleaned_data[k] for k in private_data_list if k in cleaned_data
        }

        return cleaned_data


class OmeroConnector(Mapper):
    def __init__(self, tool, token, user_vault_id=None):
        self.url = tool.get_public_param("url")
        self.login = tool.get_private_param("login", token, user_vault_id)
        self.password = tool.get_private_param("password", token, user_vault_id)
        self.tool = tool

    @classmethod
    def get_name(cls):
        return "Omero"

    @classmethod
    def get_data_structure(cls):
        return openlink.core.connector.LIST_STRUCTURE

    @classmethod
    def has_access_url(cls):
        return True

    @classmethod
    def get_supported_types(cls, data_object=None, tool=None):
        list_supported_types = {
            "study": ["project"],
            "assay": ["dataset"],
            "data": ["image"],
        }
        if data_object:
            parent = data_object.get_parent(data_object.id)
            try:
                Mapping.objects.get(foreign_id_obj=parent[0], tool_id=tool)
            except Mapping.DoesNotExist:
                if data_object.__class__.__name__.lower() != "study":
                    list_supported_types.pop(
                        data_object.__class__.__name__.lower(), None
                    )
        return list_supported_types

    @classmethod
    def get_addlink_form(cls, data, *args, **kwargs):
        return OmeroEditCredentials(data, *args, **kwargs)

    def get_plural_form_of_type(cls, type):
        plural_form = {"project": "projects", "dataset": "datasets", "image": "images"}
        return plural_form[type]

    @classmethod
    def get_creation_form(cls, data=None):
        return OmeroForm(data)

    @classmethod
    def get_logo(cls):
        logo = "images/logo-omero.svg"
        return logo

    @classmethod
    def get_color(cls):
        color = "#7CDD98"
        return color

    @classmethod
    def has_mapping_options(cls):
        return False

    @classmethod
    def get_port(cls):
        return 4064

    @classmethod
    def check_status_code(cls, code):
        if type(code) == PermissionDeniedException:
            raise AuthentificationError(cls, "login or password")
        elif type(code) == DNSException:
            raise ToolUnreachableError(cls)
        elif type(code) == omero.ClientError:
            raise ToolUnreachableError(cls)
        else:
            raise defaultError(cls)

    def get_host(self):
        url_parse = urlparse(self.url)
        host = url_parse.hostname
        return host

    def get_api_url(self):
        WEB_HOST = "v0/"
        url_api = "%s/api/%s" % (self.url, WEB_HOST)
        url_api = url_api.replace("//api", "/api")
        return url_api

    def get_url_link_to_an_object(self, obj_type, obj_id):
        # return a url to display for current user
        WEB_DISPLAY = "/webclient/?show="
        if obj_type == "study":
            obj_type = "project"
        if obj_type == "assay":
            obj_type = "dataset"
        if obj_type == "data":
            obj_type = "image"
        url = self.url + WEB_DISPLAY + obj_type + "-" + str(obj_id)
        return url

    def get_space_info(self, objects_id, mapping_object=None):
        import omero
        if mapping_object:
            jobid = utils.start_async_task()
        try:
            client = omero.client(self.get_host(), self.get_port())
            session = client.createSession(str(self.login), str(self.password))
        except PermissionDeniedException:
            size_str = "invalid credentials"
            space = size_str
            utils.error_async_task(jobid)
            return size_str
        except omero.ClientError:
            size_str = "Error"
            space = size_str
            utils.error_async_task(jobid)
            return size_str
        except DNSException:
            size_str = "Error"
            space = size_str
            utils.error_async_task(jobid)
            return size_str
        from omero.cmd import DiskUsage2
        try:
            req = DiskUsage2()
            objects = {}
            classes = set()
            ids = [int(id) for id in objects_id]
            objects.setdefault("Image", []).extend(ids)
            import omero.callbacks

            req.targetObjects, req.targetClasses = (objects, list(classes))
            handle = session.submit(req)
            # sleep(1)
            cb = omero.callbacks.CmdCallbackI(client, handle)
            sleep(0.5)
            rsp = cb.getResponse()
            max_loop = 8
            loop = 0
            while rsp is None and loop <= max_loop:
                rsp = cb.getResponse()
                sleep(0.5)
                loop += 1
            from omero.util.text import filesizeformat

            if rsp is not None:
                size = sum(rsp.totalBytesUsed.values())
                size_str = filesizeformat(size)
            else:
                size = 0
                size_str = "0 KB"

            space = size
            if mapping_object:
                mapping_object.size = space
                mapping_object.save()
                utils.finish_async_task(jobid, space)
            else:
                return space
        except Exception as e:
            utils.error_async_task(jobid)
            return e

    def get_investigation(self, object_id):
        return None

    def get_study(self, object_id):
        client = omero.client(self.get_host(), self.get_port())
        try:
            client.createSession(str(self.login), str(self.password))
        except PermissionDeniedException:
            raise forms.ValidationError("Invalid Omero login or password")
        except omero.ClientError:
            raise forms.ValidationError("Unable to connect to Omero host")
        except DNSException:
            raise forms.ValidationError("Unable to connect to Omero host")
        conn = BlitzGateway(client_obj=client)
        conn.SERVICE_OPTS.setOmeroGroup("-1")
        project_to_link = conn.getObjects("Project", attributes={"id": object_id})
        for project in project_to_link:
            obj_to_link = ContainerDataObject(
                project.getId(), project.getName(), "project", project.getDescription()
            )
        return obj_to_link

    def get_assay(self, object_id):
        client = omero.client(self.get_host(), self.get_port())
        try:
            client.createSession(str(self.login), str(self.password))
        except PermissionDeniedException:
            raise forms.ValidationError("Invalid Omero login or password")
        except omero.ClientError:
            raise forms.ValidationError("Unable to connect to Omero host")
        except DNSException:
            raise forms.ValidationError("Unable to connect to Omero host")
        conn = BlitzGateway(client_obj=client)
        conn.SERVICE_OPTS.setOmeroGroup("-1")
        data_to_link = conn.getObjects("Dataset", attributes={"id": object_id})
        for data in data_to_link:
            obj_to_link = DataObject(
                data.getId(), data.getName(), "data", data.getDescription()
            )
        return obj_to_link

    def get_data(self, object_id):
        client = omero.client(self.get_host(), self.get_port())
        try:
            client.createSession(str(self.login), str(self.password))
        except PermissionDeniedException:
            raise forms.ValidationError("Invalid Omero login or password")
        except omero.ClientError:
            raise forms.ValidationError("Unable to connect to Omero host")
        except DNSException:
            raise forms.ValidationError("Unable to connect to Omero host")
        conn = BlitzGateway(client_obj=client)
        conn.SERVICE_OPTS.setOmeroGroup("-1")
        data_to_link = conn.getObjects("Image", attributes={"id": object_id})
        for data in data_to_link:
            obj_to_link = DataObject(
                data.getId(), data.getName(), "data", data.getDescription()
            )
        return obj_to_link

    def get_investigations(self):
        return []

    def get_studies(self, reference_mapping, container):
        projects = []
        client = omero.client(self.get_host(), self.get_port())
        try:
            client.createSession(str(self.login), str(self.password))
        except PermissionDeniedException:
            raise forms.ValidationError("Invalid Omero login or password")
        except omero.ClientError:
            raise forms.ValidationError("Unable to connect to Omero host")
        except DNSException:
            raise forms.ValidationError("Unable to connect to Omero host")
        conn = BlitzGateway(client_obj=client)
        conn.SERVICE_OPTS.setOmeroGroup("-1")
        for project in conn.listProjects():
            projects.append(
                ContainerDataObject(
                    project.getId(),
                    project.getName(),
                    "project",
                    project.getDescription(),
                )
            )
        return projects

    def get_assays(self, reference_mapping, container):
        datas = []
        client = omero.client(self.get_host(), self.get_port())
        try:
            client.createSession(str(self.login), str(self.password))
        except PermissionDeniedException:
            raise forms.ValidationError("Invalid Omero login or password")
        except omero.ClientError:
            raise forms.ValidationError("Unable to connect to Omero host")
        except DNSException:
            raise forms.ValidationError("Unable to connect to Omero host")
        conn = BlitzGateway(client_obj=client)
        conn.SERVICE_OPTS.setOmeroGroup("-1")
        for data in conn.getObjects("Dataset", opts={"project": container.id}):
            datas.append(
                DataObject(
                    data.getId(),
                    data.getName(),
                    "data",
                    data.getDescription(),
                )
            )
        return datas

    def get_datas(self, reference_mapping, container):
        datas = []
        client = omero.client(self.get_host(), self.get_port())
        try:
            client.createSession(str(self.login), str(self.password))
        except PermissionDeniedException:
            raise forms.ValidationError("Invalid Omero login or password")
        except omero.ClientError:
            raise forms.ValidationError("Unable to connect to Omero host")
        except DNSException:
            raise forms.ValidationError("Unable to connect to Omero host")
        conn = BlitzGateway(client_obj=client)
        conn.SERVICE_OPTS.setOmeroGroup("-1")
        for data in conn.getObjects("Image", opts={"dataset": container.id}):
            datas.append(
                DataObject(
                    data.getId(),
                    data.getName(),
                    "data",
                    data.getDescription(),
                )
            )
        return datas

    def get_data_objects(self, data_type, reference_mapping=None, container=None):
        if data_type == "investigation":
            get_objects = self.get_investigations()
        elif data_type == "study":
            get_objects = self.get_studies(reference_mapping, container)
        elif data_type == "assay":
            get_objects = self.get_assays(reference_mapping, container)
        elif data_type == "data":
            get_objects = self.get_datas(reference_mapping, container)
        return get_objects

    def get_data_object(self, data_type, object_id):
        if data_type == "investigation":
            get_object = self.get_investigation(object_id)
        elif data_type == "study":
            get_object = self.get_study(object_id)
        elif data_type == "assay":
            get_object = self.get_assay(object_id)
        elif data_type == "data":
            get_object = self.get_data(object_id)
        return get_object

    def download(self, object_id, target_dir):
        with cli_login(
            "%s@%s:%s" % (str(self.login), str(self.get_host()), str(self.get_port())),
            "-w",
            str(self.password),
        ) as cli:
            conn = BlitzGateway(client_obj=cli._client)
            conn.SERVICE_OPTS.setOmeroGroup("-1")
            # parent = conn.getObject("Dataset", object_id)
            # data_dir = os.path.join(target_dir, parent.name)
            os.makedirs(target_dir, exist_ok=True)
            image = conn.getObject("Image", object_id)
            # image_dir = os.path.join(target_dir, image.name)
            cli.invoke(["download", f"Image:{image.getId()}", target_dir])
            # for image in parent.listChildren():
            #     image_dir = os.path.join(data_dir, image.name)
            #     cli.invoke(["download", f"Image:{image.id}", image_dir])
        return None

    def check_file_access(self, object_id):
        with cli_login(
            "%s@%s:%s" % (str(self.login), str(self.get_host()), str(self.get_port())),
            "-w",
            str(self.password),
        ) as cli:
            conn = BlitzGateway(client_obj=cli._client)
            conn.SERVICE_OPTS.setOmeroGroup("-1")
            try:
                conn.getObject("Image", object_id)
                access = True
            except Exception as e:
                logging.debug(e)
                access = False
        return access

    def get_information(self, type, id):
        return {}


ToolConnector.register(OmeroConnector)
