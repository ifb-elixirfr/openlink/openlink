from django.apps import AppConfig


class OpenlinkOmeroConfig(AppConfig):
    name = "openlink_omero"
