from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from openlink.core.models import MappingProjectUser, Profile


def home(request):
    return render(request, "openlink/home.html")


def contact(request):
    return render(request, "openlink/contact.html")


@login_required
def get_user_profile(request, username):
    profile = Profile.objects.get(user=request.user)
    tool_list_linked = list(profile.tool_list.all())
    list_project = []
    tool_list_shared = profile.tool_list_shared.all()
    project_list = MappingProjectUser.objects.filter(user__user=request.user)
    for i in project_list:
        list_project.append(i.project)
    context = {
        "username": username,
        "profile": profile,
        "tool_list_linked": tool_list_linked,
        "list_project": list_project,
        "tool_list_shared": tool_list_shared,
    }
    return render(request, "openlink/user_profile.html", context)
