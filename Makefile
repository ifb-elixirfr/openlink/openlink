.PHONY : pylama
pylama:
	pylama openlink/core/ manage.py

.PHONY : black
black:
	black --check openlink/core/ manage.py

.PHONE : package
package:
	python setup.py sdist
